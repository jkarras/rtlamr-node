Config Directory
=========

The simple version of how to work with this directory goes as follows.

* default.json - this file contains default config values
* local.json - this file is merged with default.json replacing any values with its own. If a default value is ok leave it out of this file. This file will not be versioned by git. 

For further details on how the files in this directory work check out https://github.com/lorenwest/node-config/wiki/Configuration-Files .

These files are read by the node module [node-config](https://github.com/lorenwest/node-config/).