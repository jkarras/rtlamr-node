FROM node:0.10

RUN apt-get update && \
	apt-get install --no-install-recommends -y \
	
	build-essential \
	bzr \
	ca-certificates \
	cmake \
	curl \
	git-core \
	libusb-1.0-0-dev \
	libfftw3-dev \
	mercurial \
	pkg-config && \
	apt-get clean

# Download and install Go
RUN curl -s https://storage.googleapis.com/golang/go1.3.linux-amd64.tar.gz | tar -v -C /usr/local -xz

ENV GOROOT /usr/local/go
ENV PATH $PATH:$GOROOT/bin

ENV GOPATH /go
ENV PATH $PATH:$GOPATH/bin

# Build, test and install RTLAMR
WORKDIR /go/src/
RUN go get -v github.com/bemasher/rtlamr
RUN go test -v ./...

WORKDIR /usr/local/
RUN git clone git://git.osmocom.org/rtl-sdr.git
RUN mkdir /usr/local/rtl-sdr/build

WORKDIR /usr/local/rtl-sdr/build
RUN cmake ../ -DINSTALL_UDEV_RULES=ON -DDETACH_KERNEL_DRIVER=ON && \
	make && \
	make install && \
	ldconfig

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app

CMD [ "npm", "start" ]

EXPOSE 8081