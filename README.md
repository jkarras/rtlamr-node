# README #

This application requires that [*rtlamr*](https://github.com/bemasher/rtlamr) and [*rtl_tcp*](http://sdr.osmocom.org/trac/wiki/rtl-sdr) be installed and functional. Before running the program start [*rtl_tcp*](http://sdr.osmocom.org/trac/wiki/rtl-sdr). Modify the appropriate config file with the path to *rtlamr* and you should be good to go

### What is this repository for? ###

* Quick summary
    * https://bitbucket.org/jkarras/rtlamr-node/
* Version


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    * [rtlamr](https://github.com/bemasher/rtlamr)
    * [rtl_tcp](http://sdr.osmocom.org/trac/wiki/rtl-sdr)
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact