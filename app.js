var debug = { 
  app: require('debug')('rtlamr-node'),
  process: require('debug')('rtlamr-node:process'),
  socket: require('debug')('rtlamr-node:socket'),
  http: require('debug')('rtlamr-node:http')
};

var configfile = require('config');
var cfg = readConfig();

var RtlamrProcess = require('./rtlamr-process');
//var rtlamrProcess = RtlamrProcess.createRtlamrProcess(cfg.rtlamrExecString, cfg.rtlamrFilterId);
var rtlamrProcess = new RtlamrProcess(cfg.rtlamrExecString, cfg.rtlamrFilterId);
var app = require('http')
      .createServer(handlerHTTP)
      .listen(cfg.httpport, '0.0.0.0');
var io = require('socket.io')(app);


var fs = require('fs');
var connectCounter = 0;


//Register socket.io connection handler
io.sockets.on('connection', onSocketConnection);
debug.app('Registered socket.io connection event.');


//Start up rtlamr for the first time 
rtlamrProcess.exec();
rtlamrProcess.on('powerUpdate', onPowerUpdate);

function readConfig() {
  debug.app('parsing config file');

  //TODO add config checks to avoid messed up configs
  var config = {};
  config.rtlamrBaseDir = configfile.get('rtlamr.basedir');
  config.rtlamrCommand = configfile.get('rtlamr.command');
  config.rtlamrArgs = configfile.get('rtlamr.args');
  config.rtlamrFilterId = configfile.get('rtlamr.filterid');
  config.rtlamrExecString = config.rtlamrBaseDir + config.rtlamrCommand + ' ' + config.rtlamrArgs;

  config.httpport = configfile.get('app.httpport');
  
  debug.app('done parsing config');
  return config;
}

function handlerHTTP(req, res) {
  fs.readFile(__dirname + '/index.html', function(err, data) {
    if (err) {
      debug.http(err);
      res.writeHead(500);
      return res.end('Error loading index.html');
    }
    res.writeHead(200);
    res.end(data);
  });
}

function onSocketConnection(socket) {

   var endpoint = socket.request.connection;

  debug.socket('New connection from ' + 
    endpoint.remoteAddress + ':' + endpoint.remotePort);
  connectCounter++;
  debug.socket('NUMBER OF CONNECTIONS++: ' + connectCounter);
  socket.on('disconnect', onSocketDisconnect);
}

function onSocketDisconnect(socket) {
  debug.socket('Client disconnected');
  connectCounter--;
  debug.socket('NUMBER OF CONNECTIONS--: ' + connectCounter);

}

function onPowerUpdate(data) {
  if (typeof socket === 'undefined') {
    socket = io.sockets;
  }
  socket.emit('powerUpdate',data);
  debug.socket('emit: powerUpdate %s', JSON.stringify(data));
}
