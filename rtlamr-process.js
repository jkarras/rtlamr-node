//rtlamr-process.js
var debug = { 
  process: require('debug')('rtlamr-node:process'),
  parse: require('debug')('rtlamr-node:process.parse')
};
var EventEmitter = require( 'events' ).EventEmitter;
var util = require('util');
var cpexec = require('child_process').exec;

//Rtlamr Process constructor
function RtlamrProcess(execString, filterID) {
    if (!(this instanceof RtlamrProcess)) {
          //return createRtlamrProcess();
          return new RtlamrProcess(execString, filterID);
    }
    // Call the super constructor.
    EventEmitter.call( this );
    debug.process('ExecString: %s',execString);
    this.modExecString = execString;
    debug.process('filterID: %s', filterID);
    this.modFilterID = filterID;
    debug.process('modFilterID: %s', this.modFilterID);
    this.modRtlamrProcess = 0;
    this.onStdoutData = this._onStdoutData.bind(this);
    this.onProcessExit = this._onProcessExit.bind(this);
    this.onSterrData = this._onSterrData.bind(this);
    this.onProcessError = this._onProcessError.bind(this);
    return( this );
  }

  util.inherits(RtlamrProcess, EventEmitter);

  function createRtlamrProcess(execString, filterID) {
    return new RtlamrProcess(execString, filterID);
  }

  RtlamrProcess.prototype.exec = function rtlamrexec() {

    debug.process('Running: %s', this.modExecString);
    this.modRtlamrProcess = cpexec(this.modExecString);
//exec('~/gocode/bin/rtlamr  -quiet=true -format=json -centerfreq=910299072');

    debug.process('Started rtlamr pid: ' + this.modRtlamrProcess.pid);
    debug.process('filtering on ID: ' + this.modFilterID);
    this.modRtlamrProcess.stdout.on('data', this.onStdoutData);
    this.modRtlamrProcess.stderr.on('data', this.onSterrData);
    this.modRtlamrProcess.on('error', this.onProcessError);
    this.modRtlamrProcess.on('exit', this.onProcessExit);
  };

  RtlamrProcess.prototype._onStdoutData = function _onStdoutData(data) {
  debug.parse('JSON Parse: %s', data);

  //Occasionally we get two or more lines per event. JSON.parse can't
  //handle this. Using try/catch block to ensure things keep running for now. 
  try { 
    var bevent = JSON.parse(data);
  
  bevent.Time = new Date(Date.parse(bevent.Time)).getTime();
  if (bevent.Message.ID === this.modFilterID) {
    debug.process('emit event: \'powerUpdate\'');
    this.emit('powerUpdate',bevent);
  }
  else {
    debug.parse('filtered: %s', JSON.stringify(bevent.Message));
  }
  } catch (err) {
    debug.parse('JSON parse error %s', err);
  }
};
  

RtlamrProcess.prototype._onSterrData = function _onSterrData(data) {
  debug.process('rtlamr stderr %s', data);
};

RtlamrProcess.prototype._onProcessError = function _onProcessError(code) {
  debug.process('error condition %s', code);
};

RtlamrProcess.prototype._onProcessExit = function _onProcessExit(code) {
  debug.process('child process exited with code %s', code);
  debug.process('starting process again.');
  this.exec();
};

module.exports = RtlamrProcess;
